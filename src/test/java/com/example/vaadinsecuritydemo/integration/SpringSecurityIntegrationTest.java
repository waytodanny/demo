package com.example.vaadinsecuritydemo.integration;

import com.example.vaadinsecuritydemo.entity.Person;
import com.example.vaadinsecuritydemo.repository.PersonRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration-test")
@Transactional
public class SpringSecurityIntegrationTest {
    private static final String VALID_USER_LOGIN = "name";
    private static final String VALID_USER_PASSWORD = "password";

    //TODO
    //        SecurityMockMvcRequestBuilders
    //        MockMvcResultMatchers
    //        @WithMockUser("name")

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private PersonRepository personRepository;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();

        assert personRepository.findAll().size() == 0;

        var user = new Person(VALID_USER_LOGIN, new BCryptPasswordEncoder().encode(VALID_USER_PASSWORD));
        personRepository.save(user);
    }

    @Test
    public void whenHttpRequestToAnyPageThenRedirectToLoginPage() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/any").with(httpBasic("any", "any")))
                .andExpect(redirectedUrlPattern("**/login"));
    }

    @Test
    public void whenLoginWithValidCredentialsThenRedirectToMainPage() throws Exception {
        mvc.perform(formLogin().user(VALID_USER_LOGIN).password(VALID_USER_PASSWORD))
                .andExpect(status().isOk())
                .andExpect(authenticated().withUsername(VALID_USER_LOGIN))
                .andExpect(forwardedUrl("/main"));
    }

    @Test
    public void whenLoginWithInvalidCredentialsThenRedirectToLoginPage() throws Exception {
        mvc.perform(formLogin().user("invalid").password("invalid"))
                .andExpect(unauthenticated())
                .andExpect(redirectedUrl("/login?error"));
    }

    @Test
    public void whenLogoutThenRedirectToLoginPage() throws Exception {
        mvc.perform(logout())
                .andExpect(redirectedUrl("/login"));
    }
}
