package com.example.vaadinsecuritydemo;

import com.example.vaadinsecuritydemo.entity.Person;
import com.example.vaadinsecuritydemo.repository.PersonRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersistenceTest {

  @Autowired
  private PersonRepository repository;

  @Autowired
  private DataSource dataSource;

  @Test
  public void test() {
    repository.save(new Person("a", new BCryptPasswordEncoder().encode("a")));
    Optional<Person> a = repository.findByLogin("a");
    assertTrue(a.isPresent());
  }
}
