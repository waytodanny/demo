package com.example.vaadinsecuritydemo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@NoArgsConstructor
public class Person {

    @Id
    @GeneratedValue
    private Long id;

    private String login;

    private String password;

    public Person(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
