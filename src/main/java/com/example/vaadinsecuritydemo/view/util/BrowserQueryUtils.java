package com.example.vaadinsecuritydemo.view.util;

import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.Location;
import com.vaadin.flow.router.QueryParameters;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class BrowserQueryUtils {
    public static boolean containsParameter(String key, BeforeEvent event) {
        return getAllParameters(event).get(key) != null;
    }

    private static Map<String, List<String>> getAllParameters(BeforeEvent event) {
        return Optional.ofNullable(event)
                .map(BeforeEvent::getLocation)
                .map(Location::getQueryParameters)
                .map(QueryParameters::getParameters)
                .orElseGet(Collections::emptyMap);
    }

}
