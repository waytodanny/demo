package com.example.vaadinsecuritydemo.view;

import com.example.vaadinsecuritydemo.view.util.BrowserQueryUtils;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("login-design")
@Route("login")
@PageTitle("Login")
@HtmlImport("context://vaadin/login-design.html")
public class LoginView
        extends PolymerTemplate<LoginView.LoginDesignModel>
        implements BeforeEnterObserver {

    @Id("login-form")
    private LoginForm loginForm;

    public LoginView() {
        customizeLoginForm();
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if (BrowserQueryUtils.containsParameter("error", event)) {
            loginForm.setError(true);
        }
    }

    private void customizeLoginForm() {
        loginForm.setAction("login");
        loginForm.setVisible(true);
    }

    public interface LoginDesignModel extends TemplateModel {
    }
}
