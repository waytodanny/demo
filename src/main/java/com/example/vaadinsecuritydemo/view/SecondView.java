package com.example.vaadinsecuritydemo.view;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

@Route("s")
public class SecondView extends VerticalLayout {
    public SecondView() {
        add(new TextField("Hello User 2"));
    }
}
