package com.example.vaadinsecuritydemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaadinSecurityDemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(VaadinSecurityDemoApplication.class, args);
  }
}
